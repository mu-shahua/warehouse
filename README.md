# 仓库

#### 功能
实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。
需要将测试文本命名为123.txt进行测试。
#### 输入指令
输入c 统计字符数
输入w 统计单词数
输入s 统计句子数
#### 运行结果
输入c
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/163854_fdd02a67_8372328.png "统计字符数.png")
输入w
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/163945_3ffe95d6_8372328.png "统计单词数.png")
输入S
![输入图片说明](https://images.gitee.com/uploads/images/2020/1127/164004_2cbe668f_8372328.png "统计句子数.png")